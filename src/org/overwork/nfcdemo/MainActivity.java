package org.overwork.nfcdemo;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/** add to AndroidManifest.xml <manifest> node

    <uses-feature android:name="android.hardware.nfc" android:required="true" />

    <uses-permission android:name="android.permission.NFC" />

 */
public class MainActivity extends Activity implements View.OnClickListener {

    private NfcAdapter mNfcAdapter;

    private TextView mText_ReadFromCard;
    private EditText mEdit_WriteToCard;
    private Button mBtn_Write;

    private Tag mTag;

    @Override
    public void onClick(View v) {
        if (v == mBtn_Write) {
            String payload = mEdit_WriteToCard.getText().toString(); // 從 EditText 提出文字
            if (TextUtils.isEmpty(payload)) { // 如果是空的就閃
                return;
            }
            NdefRecord record = createMime("text/plain", payload.getBytes()); // 包成簡易文字訊息
            if (Ndef.get(mTag) != null) { // 如果 Tag 是 NDEF
                new NdefWriter(Ndef.get(mTag)).execute(record); // 用 NDEF 寫入器
            } else if (NdefFormatable.get(mTag) != null) { // 如果 Tag 是白卡
                new NdefFormatter(NdefFormatable.get(mTag)).execute(record); // 用 NdefFormatable 寫入
            } else {
                String msg = "This tag not ndef supported.";
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                mTag = null;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this); // 取得 NFC 控制器
        if (mNfcAdapter == null) { // 如果沒控制器表示手機不支援
            Toast.makeText(this, "Not NFC supported.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if (mNfcAdapter.isEnabled() == false) { // 如果未啟動 NFC
            Toast.makeText(this, "Need enable NFC.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // 要求螢幕打直
        setContentView(buildContentView()); // 建置 Activity UI
        mBtn_Write.setOnClickListener(this); // 榜定按鍵處理
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent self = new Intent(this, getClass()); // this Activity
        self.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); // run the top one
        PendingIntent intent = PendingIntent.getActivity(this, 0, self, 0); // pending for OS
        IntentFilter[] filters = { new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED) }; // filter any tag
        String[][] techLists = { { Ndef.class.getName(), NdefFormatable.class.getName() } }; // filter NDEF
        // 啟用前景調度: 當此 Activity 在最前景時 OS 探索到 NFC TAG 即轉交給此 Activity 處理
        mNfcAdapter.enableForegroundDispatch(this, intent, filters, techLists);
    }

    @Override
    protected void onPause() {
        // 取消前景調度: 當此 Activity 離開前景時 有借有還
        mNfcAdapter.disableForegroundDispatch(this);

        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) { // 如果 intent 是探索到 TAG
            mTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG); // 抽出 TAG 物件
            if (Ndef.get(mTag) != null) { // 如果是 NDEF 卡
                new NdefReader().execute(Ndef.get(mTag)); // 執行 Reader
            }
        } else {
            super.onNewIntent(intent);
        }
    }

    private View buildContentView() { // activity layout (懶得傳 XML 檔)
        LinearLayout base = new LinearLayout(this);
        base.setOrientation(LinearLayout.VERTICAL);
        base.setPadding(16, 16, 16, 16);

        ViewGroup.LayoutParams params;

        TextView text = new TextView(this);
        text.setText("Read from card:");
        params = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        base.addView(text, params);

        mText_ReadFromCard = new TextView(this);
        params = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT, 1);
        base.addView(mText_ReadFromCard, params);

        text = new TextView(this);
        text.setText("Write to card:");
        params = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        base.addView(text, params);

        FrameLayout frame = new FrameLayout(this);
        mEdit_WriteToCard = new EditText(this);
        params = new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        frame.addView(mEdit_WriteToCard, params);
        params = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT, 1);
        base.addView(frame, params);

        mBtn_Write = new Button(this);
        mBtn_Write.setText("Write");
        params = new LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
        base.addView(mBtn_Write, params);

        return base;
    }

    /** source by API 16: String android.content.Intent.normalizeMimeType(String type) */
    public static String normalizeMimeType(String type) {
        if (type == null) {
            return null;
        }

        type = type.trim().toLowerCase(Locale.US);

        final int semicolonIndex = type.indexOf(';');
        if (semicolonIndex != -1) {
            type = type.substring(0, semicolonIndex);
        }
        return type;
    }

    /** source by API 16: NdefRecord android.nfc.NdefRecord.createMime(String mimeType, byte[] mimeData) */
    public static NdefRecord createMime(String mimeType, byte[] mimeData) {
        if (mimeType == null) throw new NullPointerException("mimeType is null");

        mimeType = normalizeMimeType(mimeType);
        if (mimeType.length() == 0) throw new IllegalArgumentException("mimeType is empty");
        int slashIndex = mimeType.indexOf('/');
        if (slashIndex == 0) throw new IllegalArgumentException("mimeType must have major type");
        if (slashIndex == mimeType.length() - 1) {
            throw new IllegalArgumentException("mimeType must have minor type");
        }

        byte[] typeBytes;
        try {
            typeBytes = mimeType.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
        return new NdefRecord(NdefRecord.TNF_MIME_MEDIA, typeBytes, null, mimeData);
    }

    private static StringBuilder bytesToString(byte[] bs) {
        StringBuilder s = new StringBuilder();
        for (byte b : bs) {
            s.append(String.format("%02X", b));
        }
        return s;
    }

    private class NdefReader extends AsyncTask<Ndef, Void, NdefMessage> {

        private ProgressDialog mDialog;
        private Exception mException = null;

        @Override
        protected void onPreExecute() {
            mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setMessage("Reading...");
            mDialog.setIndeterminate(false);
            mDialog.setCancelable(false);
            mDialog.show();
        }

        @Override
        protected NdefMessage doInBackground(Ndef... params) {
            Ndef ndef = params[0];
            try {
                ndef.connect();
                return ndef.getNdefMessage();
            } catch (Exception ex) {
                mException = ex;
            } finally {
                try {
                    ndef.close();
                } catch (IOException ignore) {
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(NdefMessage result) {
            mDialog.dismiss();
            if (result != null) {
                StringBuilder buffer = new StringBuilder();
                buffer.append("Tag ID: ").append(bytesToString(mTag.getId()));
                
                buffer.append("\n\n").append("Supported technologies:");
                for (String tech : mTag.getTechList()) {
                    buffer.append("\n* ").append(tech);
                }

                buffer.append("\n\n").append("Ndef records(Payload):");
                for (NdefRecord record : result.getRecords()) {
                    buffer.append("\n* ").append(new String(record.getPayload()));
                }
                
                mText_ReadFromCard.setText(buffer.toString());
            } else {
                mText_ReadFromCard.setText("Failed:\n" + mException.getMessage());
            }
        }

    }

    private abstract class AbsNdefWriter extends AsyncTask<NdefRecord, Void, Boolean> {

        private ProgressDialog mDialog;
        protected Exception mException = null;

        @Override
        protected void onPreExecute() {
            mDialog = new ProgressDialog(MainActivity.this);
            mDialog.setMessage("Writing...");
            mDialog.setIndeterminate(false);
            mDialog.setCancelable(false);
            mDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            mDialog.dismiss();
            AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity.this);
            adb.setPositiveButton(android.R.string.ok, null);
            if (result) {
                adb.setMessage("Succeed.");
            } else {
                adb.setMessage("Failed:\n" + mException.getLocalizedMessage());
            }
            adb.show();
        }
    }

    private class NdefWriter extends AbsNdefWriter {

        private Ndef mNdef;

        public NdefWriter(Ndef ndef) {
            mNdef = ndef;
        }

        @Override
        protected Boolean doInBackground(NdefRecord... params) {
            try {
                mNdef.connect();
                mNdef.writeNdefMessage(new NdefMessage(params));
            } catch (Exception ex) {
                mException = ex;
            } finally {
                try {
                    mNdef.close();
                } catch (IOException ignore) {
                }
            }
            return null == mException;
        }

    }

    private class NdefFormatter extends AbsNdefWriter {

        private NdefFormatable mNdefFormatable;

        public NdefFormatter(NdefFormatable ndefFormatable) {
            mNdefFormatable = ndefFormatable;
        }

        @Override
        protected Boolean doInBackground(NdefRecord... params) {
            try {
                mNdefFormatable.connect();
                mNdefFormatable.format(new NdefMessage(params));
            } catch (Exception ex) {
                mException = ex;
            } finally {
                try {
                    mNdefFormatable.close();
                } catch (IOException ignore) {
                }
            }
            return null == mException;
        }
    }
}
